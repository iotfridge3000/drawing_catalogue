const AWS = require("aws-sdk");
const fs = require("fs");
const util = require("util");

require("dotenv").config();

const s3 = new AWS.S3({
  region: process.env["SPACES_REGION"],
  endpoint: `https://${process.env["SPACES_REGION"]}.digitaloceanspaces.com`,
  accessKeyId: process.env["SPACES_KEY"],
  secretAccessKey: process.env["SPACES_SECRET"],
  sslEnabled: true
});

const readdir = util.promisify(fs.readdir);
const readFile = util.promisify(fs.readFile);

async function uploadFiles(folderName){
  try {
    const uploadDir = `${__dirname}/${folderName}`;
    const fileNamesToUpload = await readdir(uploadDir);
    const putObjectPromiseArray = fileNamesToUpload.map(async fileName => {
      try {
        const payload = await readFile(`${uploadDir}/${fileName}`)
        const putObjParams = {
          Bucket: process.env["DO_BUCKET"],
          Key: fileName,
          Body: payload
        }
        return s3.putObject(putObjParams).promise();
      } catch (e) {
        console.log(e);
      }
    });
    const fulfillPromiseUploadArray = await Promise.all(putObjectPromiseArray);
    console.log(fulfillPromiseUploadArray);
  } catch (e) {
    console.log(e) 
  }
}

uploadFiles(process.argv[2]);
